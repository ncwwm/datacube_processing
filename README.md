# Hyperspectral DataCube Processing

This package provides a class for processing hyperspectral datacubes. The class is named `DataCube` and has several methods for performing different operations on the datacube. 
This package has been created for a custom processing of data-cubes collected for research purpose. It might requires adaptation to be more flexible. 

## Installation

To install the package, run the following command:


```shell
git clone https://gitlab.switch.ch/ncwwm/datacube_processing.git
```

```shell
pip install wwdatacube
```

## Usage

Here is an example of how to use the `DataCube` class:

```python
from wwdatacube import DataCube

## Initialize the DataCube object
img_name = "example_image"
img_path = "path/to/image/"
datacube = DataCube(img_name, img_path)

# Convert the image to reflectance
datacube.convert_to_reflectance()

# Reframe the image
datacube.custom_reframe()

# Apply the mask to the image
datacube.apply_mask()

# Extract the mean and standard deviation spectra from the image
mean, std = datacube.reflectance_spectra_extraction()

# Display the image using the specified RGB bands
datacube.show([0, 1, 2])
```

## Testing

First you need to install `pytest`, which is listed in the developper requirements

```shell
pip install pytest
```

Running the tests:
```shell
pytest
```


## Dependencies

This package depends on the following python packages:
* `numpy==1.24.1`
* `spectral==0.23.1`
* `matplotlib==3.6.3`