from wwdatacube import DataCube
import os
import pytest

def test_datacube(img_test):
    dir_path = os.path.dirname(__file__)
    img_path = dir_path + '\\test_data\\'
    img_name = '2_3_05ml'
    dc = DataCube(img_name, img_path)

    assert (dc.img == img_test).all()
    assert dc.converted_to_reflectance == False
    assert dc.image_reframed == False
    assert dc.mask_applied == False

def test_datacube_convert_to_reflectance():
    dir_path = os.path.dirname(__file__)
    img_path = dir_path + '\\test_data\\'
    img_name = '2_3_05ml'
    dc = DataCube(img_name, img_path)
    dc.convert_to_reflectance()

    assert dc.converted_to_reflectance

def test_datacube_reframe():
    dir_path = os.path.dirname(__file__)
    img_path = dir_path + '\\test_data\\'
    img_name = '2_3_05ml'
    dc = DataCube(img_name, img_path)
    dc.custom_reframe()

    assert dc.image_reframed

def test_datacube_apply_mask():
    dir_path = os.path.dirname(__file__)
    img_path = dir_path + '\\test_data\\'
    img_name = '2_3_05ml'
    dc = DataCube(img_name, img_path)
    dc.apply_mask()

    assert dc.converted_to_reflectance
    assert dc.image_reframed
    assert dc.mask_applied