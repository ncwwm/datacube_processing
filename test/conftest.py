from spectral import envi
import pytest
import os

@pytest.fixture
def img_test():
    dir_path = os.path.dirname(__file__)
    img_path = dir_path+'\\test_data\\'
    img_name = '2_3_05ml'
    img = envi.open(img_path + img_name + '.hdr', img_path + img_name + '.bin').load()
    return img
