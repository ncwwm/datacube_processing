from wwdatacube.small_function import mask_80_percentile_reflection, mask_20_percentile_reflection, \
    mask_application, calc_mean_std_img
import numpy as np
import pytest


def test_mask_80_percentile_reflection(img_test):
    img = img_test
    r = mask_80_percentile_reflection(img)
    assert isinstance(r, np.ndarray)
    assert r.shape == img.shape
    assert (r == 0).any()
    assert (r == 1).any()


def test_mask_80_percentile_reflection_with_invalid_input():
    img = np.random.rand(2,2)
    with pytest.raises(AssertionError):
        r = mask_80_percentile_reflection(img)


def test_mask_20_percentile_reflection(img_test):
    img = img_test
    r = mask_20_percentile_reflection(img)
    assert isinstance(r, np.ndarray)
    assert r.shape == img.shape
    assert (r == 0).any()
    assert (r == 1).any()


def test_mask_20_percentile_reflection_with_invalid_input():
    img = np.random.rand(2,2)
    with pytest.raises(AssertionError):
        r = mask_20_percentile_reflection(img)


def test_mask_application(img_test):
    img = img_test
    r = mask_application(img)
    assert isinstance(r, np.ndarray)
    assert r.shape == img.shape
    assert (r == 0).any()


def test_calc_mean_std_img():
    img = np.ones([3, 3, 3])
    m, s = calc_mean_std_img(img)
    assert (m == 1).all()
    assert (s == 0).all()