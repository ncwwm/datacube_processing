from setuptools import setup, find_packages

setup(
    name='wwdatacube',
    version='1.0.1',
    packages=find_packages(),
    url='https://gitlab.switch.ch/ncwwm/datacube_processing',
    license='MIT',
    author='<Lechevallier Pierre>',
    author_email='<pierre.lechevallier@eawag.ch>',
    description='A package for processing hyperspectral datacubes',
    install_requires=['numpy==1.24.1', 'spectral==0.23.1','matplotlib==3.6.3']
)