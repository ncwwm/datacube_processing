import numpy as np
     
    
def mask_80_percentile_reflection(img: np.ndarray):
    """
    Creates a reflection mask for an image based on the 80th percentile of pixel values.
    
    Parameters:
    img (ndarray): An image represented as a 3-dimensional NumPy array.
    
    Returns:
    ndarray: A reflection mask represented as a 3-dimensional NumPy array.
    """
    # Testing the shape of the input
    assert(len(img.shape) == 3), 'The input needs to ne a 3D numpy array'
    
    # Calculate the 80th percentile of pixel values
    perc80 = np.percentile(img, 80, axis=(0,1)).mean()
    
    # Create an array with the 80th percentile value
    med_array = perc80 * np.ones((img.shape[0], img.shape[1]))
    
    # Create a boolean array indicating which pixels are above the 80th percentile
    refle = img.mean(axis=2) > med_array
    
    # Create an array of zeros with the same shape as refle
    r = np.zeros([len(refle), len(refle[1])])
    
    # Set the elements of r to 1 where refle is False
    r[np.logical_not(refle)] = 1
    
    # Repeat r along the third axis to match the shape of img
    r = np.repeat(r[:,:,np.newaxis], img.shape[2], axis=2)
    
    return r

def mask_20_percentile_reflection(img):
    """
    Creates a reflection mask for an image based on the 20th percentile of pixel values.
    
    Parameters:
    img (ndarray): An image represented as a 3-dimensional NumPy array.
    
    Returns:
    ndarray: A reflection mask represented as a 3-dimensional NumPy array.
    """
    # Testing the shape of the input
    assert (len(img.shape) == 3), 'The input needs to ne a 3D numpy array'
    
    # Calculate the 20th percentile of pixel values
    perc20 = np.percentile(img, 20, axis=(0,1)).mean()
    
    # Create an array with the 20th percentile value
    med_array = perc20 * np.ones((img.shape[0], img.shape[1]))
    
    # Create a boolean array indicating which pixels are below the 20th percentile
    refle = img.mean(axis=2) < med_array
    
    # Create an array of zeros with the same shape as refle
    r = np.zeros([len(refle), len(refle[1])])
    
    # Set the elements of r to 1 where refle is False
    r[np.logical_not(refle)] = 1
    
    # Repeat r along the third axis to match the shape of img
    r = np.repeat(r[:,:,np.newaxis], img.shape[2], axis=2)
    
    return r

def calc_mean_std_img(img):
    """
    Calculation of the mean reflectance spectrum and the standard deviation of an image
    """

    # Testing the shape of the input
    assert (len(img.shape) == 3), 'The input needs to ne a 3D numpy array'

    # getting the spectra
    sp_list = []

    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if np.all(img[i][j] != 0):
                sp_list.append(img[i][j])

    sp_list = np.array(sp_list)

    # calculating the mean spectra with standard deviation
    m = sp_list.mean(axis=0)
    s = sp_list.std(axis=0)

    return m, s

def mask_application(img):

    # Testing the shape of the input
    assert (len(img.shape) == 3), 'The input needs to ne a 3D numpy array'
    
    # calculation of each mask
    r = mask_80_percentile_reflection(img)
    r2 = mask_20_percentile_reflection(img)
    
    # Combination of both masks
    r3 = r*r2
    
    # application of the masks to the image
    return r3*img
