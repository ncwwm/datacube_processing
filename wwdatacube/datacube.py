from spectral import envi, imshow
from .small_function import mask_application, calc_mean_std_img
import numpy as np
import matplotlib.pyplot as plt

class DataCube:
    """A class for processing hyperspectral datacubes.

    Attributes:
        img (ndarray): The image data of the datacube.
        __name (str): The name of the image file.
        converted_to_reflectance (bool): Indicates if the image has been converted to reflectance.
        image_reframed (bool): Indicates if the image has been reframed.
        mask_applied (bool): Indicates if the mask has been applied to the image.
        __source (str): The path to the image file.
    """
    
    __slots__ = ['img', '__name', 'converted_to_reflectance', 'image_reframed', 'mask_applied', '__source']
    
    def __init__(self, img_name: str, img_path: str):
        """Initializes a DataCube object with the specified image and path.
        
        Args:
            img_name (str): The name of the image file.
            img_path (str): The path to the image file.
        """
        self.img = envi.open(img_path + img_name + '.hdr', img_path + img_name + '.bin').load()
        self.__name = img_name
        self.converted_to_reflectance = False
        self.image_reframed = False
        self.mask_applied = False
        self.__source = img_path
    
    def convert_to_reflectance(self):
        """Converts the image to reflectance if it has not already been normalized. It requires the presence of a dark reference and white reference (ENVI format: .hdr and .bin) in the img_path folder"""
        if not self.converted_to_reflectance:
            white_ref = envi.open(self.__source + 'whiteReference.hdr', self.__source + 'whiteReference.bin').load()
            dark_ref = envi.open(self.__source + 'darkReference.hdr', self.__source + 'darkReference.bin').load()

            self.img = (self.img - dark_ref)/(white_ref - dark_ref)
            self.converted_to_reflectance = True
            
    def custom_reframe(self):
        """Custom reframing of the image"""
        if not self.image_reframed:
            self.img = self.img[0:45, 110:860, 10:291]
            self.image_reframed = True

    def reframe(self, new_frame:list):
        assert len(new_frame) == 3
        for i in range(3):
            assert len(new_frame[i]) == 2
            assert new_frame[i][0] < new_frame[i][1]
            assert new_frame[i][0] >= 0
            assert new_frame[i][1] <= self.img.shape[i]

        self.img = self.img[new_frame[0][0]:new_frame[0][1],new_frame[1][0]:new_frame[1][1],new_frame[2][0]:new_frame[2][1]]



    def apply_mask(self):
        """Application of the mask after re-framing and normalization"""
        # conversion to reflectance
        if not self.converted_to_reflectance:
            self.convert_to_reflectance()
            
        # Re-framing the data-cubes to remove border and make them all have the same shape
        if not self.image_reframed:
            self.custom_reframe()
        
        # application of the mask to remove bad pixels
        if not self.mask_applied:
            self.img = mask_application(self.img)
            self.mask_applied = True
            
            
    def reflectance_spectra_extraction(self, plot=False):
        """Extracts the mean and standard deviation spectra from the image.
        
        Returns:
            A tuple containing the mean and standard deviation spectra.
        """
        
        # extraction of the mean spectrum with standard deviation
        mean, std = calc_mean_std_img(self.img)

        if plot:
            plt.plot(mean)
            plt.show()

        return mean, std
    
    def show(self, rgb_index: list = [], figsize: tuple = (20,10)):
        """Displays the image using the specified RGB bands.

        Args:
            rgb_index (list, optional): A list of indices specifying the bands to use for the red, green, and blue channels. If gefaults to [], rbg_index = [int(0.25*l), int(0.5*l), int(0.75*l)] with l the numbe rof bands.
            figsize (tuple, optional): A tuple specifying the size of the figure to display. Defaults to (20,10).
        """
        if not rgb_index:
            l = np.shape(self.img)[2]
            rbg_index = [int(0.25*l), int(0.5*l), int(0.75*l)]
        
        imshow(self.img, rgb_index, figsize=figsize)